#!/bin/bash

# ======= Functions =========

# ------- atd pkg installation ---------

atdf-centos-redhat(){
	yum install at -y
	service atd start
	service atd status
}

atdf-ubuntu(){
	apt-get install at -y
	systemctl start atd
	systemctl status atd
}
	
# -------- Reading the information ----------

reading_info(){
	echo "Please enter the Work Order or REQ number: "
	read wo

	echo "Please enter the -a account which you want to add to the server: "
	read acc

	echo "How long do you need this permission (Please define the number of days. up to 14 days) : "
	read length
}

# ------ Checking the OS Version --------

#os_v(){
	#hostnamectl | grep -io centos | tr [:lower:] [:upper:] | sort -u > ./os-version.tmp 2> /dev/null
	#hostnamectl | grep -io ubuntu | tr [:lower:] [:upper:] | sort -u >> ./os-version.tmp 2> /dev/null
	#hostnamectl | grep -io "red hat" | tr [:lower:] [:upper:] | sort -u >> ./os-version.tmp 2> /dev/null
	#expr osver=$(hostnamectl | grep -i operating | cut -d " " -f5 | tr [:lower:] [:upper:] | sort -u)
#}

# ------ Installing the package(s) ---------

installing_pkg(){
		osver=$(hostnamectl | grep -i operating | cut -d " " -f5 | tr [:lower:] [:upper:] | sort -u)
		if [[ $osver == "CENTOS" || "RED" ]]
        	then
               	atdf-centos-redhat
		elif [[ $osver == "UBUNTU" || "DEBIAN" ]]
        	then
                atdf-ubuntu
		else
        	echo "Unknown OS Version. Please do it manually."
	    fi
}

# ------ Creating the file(s) and adding the permissions ---------

file_creation(){
	if [[ ("$wo" != "") && ("$wo" != " ") ]]
	then
		touch /etc/sudoers.d/"$wo"
		chmod 440 /etc/sudoers.d/"$wo"
		echo -e "#$wo \n" > /etc/sudoers.d/"$wo"
		echo "$acc ALL=(ALL) ALL" >> /etc/sudoers.d/"$wo"
	else
		echo "The permission hasn't been granted. Please try again."
	fi
}

# ----- Adding the job to atd ---------------

at_job(){
	if [[ $length -ge 14 ]]
	then
		echo "rm -f /etc/sudoers.d/$wo" | at now + 14 days
	else
		echo "rm -f /etc/sudoers.d/$wo" | at now + $length days
	fi
	echo -e "============================= \nHere is the due time: "
	atq
	echo -e "\n============================="
}


# ======= Main ===========


reading_info
#os_v
installing_pkg
file_creation
at_job
visudo -c
sudo -l -U $acc