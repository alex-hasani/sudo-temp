#!/bin/bash

sudo su itans
while read line
    do
        echo -e "\n *** Run this command: sudo su - root -c 'bash /home/itunix/temp_sudo.sh' ***\n Use the information below:\n "
        echo "1. Workorder Number : $1"
        echo "2. Username : $2"
        echo "3. Length (Days): $3"
        echo "====================================================================================="
        #ssh -n $line "sudo su - root -c 'chmod 770 /home/itunix/temp_sudo.sh'"
        ssh -n -T $line "sudo su - root -c 'bash /home/itunix/temp_sudo.sh'"
done < servers.txt