#!/bin/bash

# -------- Reading the information ----------

reading_info(){
	echo "Please enter the Work Order or REQ number: "
	read wo

	echo "Please enter the -a account which you want to add to the server: "
	read acc

	echo "How long do you need this permission (Please define the number of days. up to 14 days) : "
	read length
}

reading_info
while true
do
    echo -e " Enter a number : \n 1. Create a user \n 2. Copy the script to target server(s) \n 3. Run the script \n 4. Delete the script from the target server \n"
    read var
    case $var in
    1)
        ./usercreation.sh $acc
        ;;
    2)
        ./scp.sh   
        ;;
    3)
        ./remoteexec.sh $wo $acc $length
        ;;
    4)
        ./deletion.sh   
        ;;
    esac
done