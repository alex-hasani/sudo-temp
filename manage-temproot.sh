#!/bin/bash

## [itans@lvsctdev002 ~]$ date "+%s"
## 1577627990
## [itans@lvsctdev002 ~]$ date --date='@1577627990'
## Sun Dec 29 06:59:50 -07 2019

export PATH=/usr/bin:/bin:/usr/sbin:/sbin

TSUDO="/etc/sudoers.d/temproot"
REVIEW_TOOL="/usr/local/bin/review_temproot.sh"
CRONJOB="45 * * * * ${REVIEW_TOOL} >/dev/null 2>&1"

PRG=$(basename $0)

print_usage()
{
echo -e "
$PRG - manage temporary root privilege on a customized sudoers file - \"$TSUDO\"

Usage: 
	$PRG -a|-e user -p duration -L request
	$PRG -d|-r user
	$PRG -l [user]
	$PRG -v

Options and Parameters:
	-a add a user with duration in the unit of day, an existing 
		valid enabled entry of the user is to be disabled if any
	-d disable a user's entries, regardless valid or invalid
	-e extend duration for a user with a valid enabled entry
	-l show user information in valid entries
	-p duration in the unit of day 
	-r remove disabled entries for a user
	-v verify entries

	options \"-a\" & \"-e\" are available only for a valid user.

	Only one valid enabled entry is allowed for a user on $TSUDO
"

if [ ! -z $tf ]; then rm -f $tf; fi

}


add_user()
{
if [ -f $TSUDO ]; then
	grep -qE "^${user}+\s+ALL\s*=\s*\(ALL\)\s*ALL\s+#\s+[0-9]+[\s,]+" $TSUDO
	if [ $? -eq 0 ]; then
		echo "Disabling entries of \"$user\"..."
		grep -E "^${user}\s+ALL\s*=\s*\(ALL\)\s*ALL\s+#\s+[0-9]+[\s,]+" $TSUDO
		sed -i -E "/^(${user}\s+ALL\s*=\s*\(ALL\)\s*ALL\s+#\s+[0-9]+[\s,]+.*)$/s//# \1/" $TSUDO
	fi
else
	echo -e "#comment: manual modification to this file is not encouraged
#comment: try to manipulate this file by /usr/local/bin/${PRG}

#comment: format of a valid entry
#comment: userid ALL=(ALL) ALL # no_of_seconds_since_1970, DATE, REQUEST_ID
" > $TSUDO; chmod 440 $TSUDO
fi
		
typeset -i now=$(date "+%s")
typeset -i ifuture=$now+86400*${days}
sfuture=$(date --date="@${ifuture}")
echo -e "\nAdding an entry for \"$user\"..."
echo -e "$user\tALL=(ALL) ALL # ${ifuture}, ${sfuture}, ${request}"
echo -e "$user\tALL=(ALL) ALL # ${ifuture}, ${sfuture}, ${request}" >> $TSUDO

echo -e "\nVerifying sudoers..."
visudo -c

if [ $? -ne 0 ]; then exit 1; fi

schedule_job

}

disable_user()
{
if [ ! -f $TSUDO ]; then
	echo -e "Error: $TSUDO is missing." >&2
	return
fi

echo -e "\nDisabling entries for \"$user\"..."
grep -E  "^${user}\s+" $TSUDO
sed -i -E "/^(${user}\s+.*)$/s//# \1/" $TSUDO

echo -e "\nVerifying sudoers..."
visudo -c
}

extend_duration()
{
if [ ! -f $TSUDO ]; then
	echo -e "Error: $TSUDO is missing." >&2
	return
fi

grep -qE "^${user}\s+ALL\s*=\s*\(ALL\)\s*ALL\s+#\s+[0-9]+[\s,]+" $TSUDO
if [ $? -ne 0 ]; then
	echo -e "Error: No valid enabled entries for user \"$user\"" >&2
	return
fi

typeset -i now=$(grep -E "^${user}\s+ALL\s*=\s*\(ALL\)\s*ALL\s+#\s+[0-9]+[\s,]+" $TSUDO | awk -F"#" '{print $2}' | sed -e 's/,/ /' | awk '{print $1}')
typeset -i ifuture=$now+86400*${days}
sfuture=$(date --date="@${ifuture}")

echo -e "\nDisabling an entry for \"$user\"..."
grep -E "^${user}\s+ALL\s*=\s*\(ALL\)\s*ALL\s+#\s+[0-9]+[\s,]+" $TSUDO
sed -i -E "/^(${user}\s+ALL\s*=\s*\(ALL\)\s*ALL\s+#\s+[0-9]+[\s,]+.*)/s//# \1/" $TSUDO

echo -e "\nAdding an entry for \"$user\"..."
echo -e "$user\tALL=(ALL) ALL # ${ifuture}, ${sfuture}, ${request}"
echo -e "$user\tALL=(ALL) ALL # ${ifuture}, ${sfuture}, ${request}" >> $TSUDO

echo -e "\nVerifying sudoers..."
visudo -c
	
if [ $? -ne 0 ]; then exit 1; fi
echo -e "\nSchudling a cron job..."
schedule_job
}

remove_entry()
{
if [ ! -f $TSUDO ]; then
	echo -e "Error: $TSUDO is missing." >&2
	return
fi

echo -e "Removing disabled entries for \"$user\"..."
grep -E  "^#\s*${user}\s+" $TSUDO
sed -i -E "/^#\s*${user}\s+/d" $TSUDO

echo -e "\nVerifying sudoers..."
visudo -c
}

show_info()
{
if [ ! -f $TSUDO ]; then
	echo -e "Error: $TSUDO is missing." >&2
	return
fi

echo -e "Show valid entries on ${TSUDO}..."
if [ "X${user}" != "X" ]; then
	grep -E "^(#\s*)?${user}\s+ALL\s*=\s*\(ALL\)\s*ALL\s+#\s+[0-9]+[\s,]+" $TSUDO > $tf
else
	grep -E "^(#\s*)?[[:alnum:]_-]+\s+ALL\s*=\s*\(ALL\)\s*ALL\s+#\s+[0-9]+[\s,]+" $TSUDO > $tf
fi

while IFS= read -r line; do
	if echo $line | grep -q "^#"; then 
		status="disabled"
		line=$(echo $line | sed -E "s/^#\s*//")
	else
		status="enabled\t"
	fi
	
	show_entry "$line" ${status}

done < $tf
}

show_entry()
{
entry="$1"
status=$2
user=$(echo $entry | awk '{print $1}')
mydate=$(echo $entry | awk -F"#" '{print $2}' | sed -e "s/,/ /" | awk '{print $1}')
echo -e "Status: ${status}\tUser: ${user}\tExpire Date: \c"; date --date="@${mydate}"
}

verify_users()
{
if [ ! -f $TSUDO ]; then
	echo -e "Error: $TSUDO is missing." >&2
	return
fi


echo -e 'Listing comment lines started with "#comment:"...'
grep -E "^#comment:" $TSUDO

echo -e "\nListing disabled entries..."
grep -E "^#\s*[[:alnum:]_-]+\s+ALL\s*=\s*\(ALL\)\s*ALL\s+#\s+[0-9]+[\s,]+" $TSUDO

echo -e "\nListing enabled entries..."
grep -E "^[[:alnum:]_-]+\s+ALL\s*=\s*\(ALL\)\s*ALL\s+#\s+[0-9]+[\s,]+" $TSUDO

echo -e "\nListing invalid entries..."
grep -v "^\s*$" $TSUDO | grep -vE "^(#comment:|(#\s*)?[[:alnum:]_-]+\s+ALL\s*=\s*\(ALL\)\s*ALL\s+#\s+[0-9]+[\s,]+)" 
rc=$?

echo -e "\nVerifying sudoers..."
visudo -c

if [ $? -ne 0 -o $rc -eq 0 ]; then 
	echo -e "\nVerification failed"
	return 1
else
	echo -e "\nVerification passed"
	return 0
fi

}

schedule_job()
{

echo -e "\ncreating a tool for reviewing temp root configuration..."

if [ -x $REVIEW_TOOL ]; then
	echo -e "  regenerated -\t\c"
else
	echo -e "  created -\t\c"
fi

cat - > $REVIEW_TOOL << EOF_REVIEW_TEMPROOT
#!/bin/bash

export PATH=/usr/bin:/bin:/usr/sbin:/sbin

SUDO_FILE="/etc/sudoers.d/temproot"

tf="/tmp/review_temproot.tmp"

if [ ! -f \$SUDO_FILE ]; then exit 0; fi

grep -qE "^[[:alnum:]_-]+\s+ALL\s*=\s*\(ALL\)\s*ALL\s+#\s+[0-9]+[\s,]+" \$SUDO_FILE
if [ \$? -ne 0 ]; then exit 0; fi

grep -E "^[[:alnum:]_-]+\s+ALL\s*=\s*\(ALL\)\s*ALL\s+#\s+[0-9]+[\s,]+" \$SUDO_FILE > \$tf

while IFS= read -r line; do
	user=\$(echo \$line | awk '{print \$1}')
	typeset -i expire_time=\$(echo \$line | awk -F"#" '{print \$2}' | sed -e 's/,/ /' | awk '{print \$1}')

	typeset -i now=\$(date "+%s")
	if [ \$now -ge \$expire_time ]; then
		echo -e "\$(date) -  disabling \"\${line}\" on \$SUDO_FILE" >> /var/log/review_temproot.log
		sed -i -E "/^(\${user}\s+ALL\s*=\s*\(ALL\)\s*ALL\s+#\s+\${expire_time}[\s,]+.*)$/s//# \1/" \$SUDO_FILE
	fi
done < \$tf

rm -f \$tf

exit 0
EOF_REVIEW_TEMPROOT

chmod 500 $REVIEW_TOOL
echo "$REVIEW_TOOL"

echo -e "\nScheduling a cron job..."
crontab -l > $tf
grep -vE "^\s*(#.*)?$" $tf | awk '{print $6}' | grep -qE "^${REVIEW_TOOL}$"
if [ $? -ne 0 ]; then
	echo "${CRONJOB}" >> $tf	
	crontab $tf
	if [ $? -ne 0 ]; then
		echo "Error: failed to schedule a cron job - $CRONJOB"
		exit 1
	else
		echo -e "  scheduled - \t${CRONJOB}"
	fi
else
	echo -e "  skipped -\t\c"
	grep -vE "^\s*(#.*)?$" $tf | grep -E "\s${REVIEW_TOOL}\b"
fi

}

# MAIN 

typeset -i days=0
action=""
user=""
request=""


while [ "X$1" != "X" ]; do
  case $1 in
	'-a')
		shift
		action="add"
		user=$1
		shift
		;;
	'-d')
		shift
		action="disable"
		user=$1
		shift
		;;
	'-e')
		shift
		action="extend"
		user=$1
		shift
		;;
	'-r')
		shift
		action="remove"
		user=$1
		shift
		;;
	'-l')
		shift
		action="list"
		user=$1
		shift
		;;
	'-v')
		shift
		action="verify"
		user=$1
		shift
		;;
	'-p')
		shift
		days=$1
		shift
		;;
	'-L')
		shift
		request="$1"
		shift
		;;
	*)
		print_usage >&2
		exit 1
		;;
  esac
done

tf=$(mktemp)
if [ $? -ne 0 ]; then exit 3; fi

if [ "X${action}" == "X" ]; then print_usage >&2; exit 1; fi

case $action in 
	"add" )
		if [ "X${user}" == "X"  -o $days -eq 0 -o "X${request}" == "X" ]; then print_usage; exit 1; fi
		if id $user >/dev/null 2>&1; then
			add_user $user $days "$request"
		else
			echo -e "Error: invalid user \"$user\"" >&2; print_usage; exit 2
		fi
		;;
	"disable" )
		if [ "X${user}" == "X" ]; then print_usage; exit 1; fi
		#if id $user >/dev/null 2>&1; then
			disable_user $user
		#else
		#	echo -e "Error: invalid user \"$user\"" >&2; print_usage; exit 2
		#fi
		;;
	"extend" )
		if [ "X${user}" == "X"  -o $days -eq 0 -o "X${request}" == "X" ]; then print_usage; exit 1; fi
		if id $user >/dev/null 2>&1; then
			extend_duration $user $days "$request"
		else
			echo -e "Error: invalid user \"$user\"" >&2; exit 2
		fi
		;;
	"remove" )
		if [ "X${user}" == "X" ]; then print_usage; exit 1; fi
		remove_entry $user
		;;
	"list" )
		if [ "X${user}" != "X" ]; then 
		#	if id $user >/dev/null 2>&1; then
				show_info $user
		#	else
		#		echo -e "Error: invalid user \"$user\"" >&2; exit 2
		#	fi
		else
			show_info
		fi
		;;
	"verify" )
		verify_users
		;;
	*)
		print_usage >&2
		exit 1
		;;
esac

rm -f $tf

